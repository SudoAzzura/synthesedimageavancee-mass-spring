# TD3 - Chloé Neuville

## 2. Complétez les méthodes addForces pour les 3 classes GravityForce, DragForce et SpringForce.
J'ai eu des problèmes au début pour l'ajout des SpringForce car il faut en ajouter deux forces à deux particules différentes. Il fallait ajouter une force à l'une puis la force inverse à l'autre, et moi j'ajoutais toutes les forces de la première à la seconde, c'est certain qu'au fur et à mesure les ressorts finissaient par subir trop de force qu'ils n'étaient pas sencé subir et s'en allait.
![alt text](./dataMd/bugForces.png)


## 5. Expérimentez avec différents pas de temps (_dt) et en jouant avec les paramètres des ressorts.
Quand j'augmente le coefficient de viscosité l'animation est de plus en plus lente, le fait que ce soit plus visqueux rend le replacement des ressort plus lent.
J'ai aussi tenté d'augmenter la masse des particules pour que celles-ci mettent moins de temps à atteindre la position initiale, c'était bon mais le tissu avait l'air lourd et donc était plus sensible la gravité et tiré vers le bas, je me suis rendue compte qu'en modifiant plutôt le pas de temps ça faisait la même chose et sans la sensation de lourdeur.En augmentant le coefficient d'amortissemant le tissu se colle sur lui-même et met du temps à redescendre, la valeur de base 200 est très bien.
![alt text](./dataMd/bugCollé.png)
la version la plus intéressante que j'ai trouvé est avec un coefficient de viscosité(kd) à 3 au lieu de 0,1, un coefficient d'amortissement(ks) de 200 et un delta t de 0.0006 au lieu de 0.0001. Je trouve que c'est le test le plus réaliste mais des bugs où la structure se colle à elle même subsistent, des fois ils sont là et d'autres fois non.
Ces bugs arrivent lorsque je tire le tissu vers le haut lorsqu'il redescent le bug se produit.
![alt text](./dataMd/bugCollé2.png)

## 6. Pour augmenter la stabilité du tissu et éviter les cisaillement, ajoutez des ressorts le long de la diagonale des cellules de la grille. Quel longueur au repos faut-il choisir ?

La longueur au repos qu'il faut choisir est celle de l'hypothénuse d'un triangle rectangle c'est à dire la racine de la somme des carré des longueur de ses sommets.
En fonction, du côté où l'on touche le tissu, où l'on essaye de le déformer, ça ne donne pas la même chose. 
Lorsque je test le modèle directement sur la situation qui m'est présenté quand je lance la simulation, il y a ce problème lorsque je fait une déformation au centre du tissu en le tirant vers le haut: 

![alt text](./dataMd/bugcenter.png)

Mais lorsque je retourne le modèle, il n'y a plus ce problème et avec un delta t de 0.0003 la simulation est super réaliste.
Et plus je bouge le tissu moins il y a de bug où il se colle sur lui même.
Sur ce screen, je maintiens le côté gauche plié puis sur le screen suivant il est revenu à sa place initiale.
![alt text](./dataMd/grid_folded_side.png)
![alt text](./dataMd/grid_return_to_begin.png)

Sur ce nouveau screen, je maintiens le centre tiré vers le haut puis sur le screen suivant il est revenu à sa place initiale. C'est dans cette situation qu'il y avait le plus de problème.

![alt text](./dataMd/grid_folded_center.png)
![alt text](./dataMd/grid_return_to_begin2.png)


## 7. Implémentez la méthode du point milieu. Observez son effet sur la stabilité et les performances en faisant varier le pas de temps.

Il n'y a plus du tout de problème, là où le maillage se collait sur lui même, il se repose délicatement à sa place .
Par contre, pour dt = 0.011, le maillage devient peu à peu instable, il fait "coucou", les ressort sur les côté en haut "vibrent", et si je bouge le maillage trop haut à droite ou à gauche, jusqu'au dessus des ressort qui "buggent", le maillage quitte la scene.
Et à dt > 0.011, le maillage quitte la scene dès le début. Sinon si on prend par exemple, à dt = 0.0009 tout est parfait.
## 8. Ajoutez un plan pour représenter le sol de la scène.
Lorsque je créée un plan représentant le sol de la scène, celui-ci était noir car je n'avais pas ajouté de lumière pour l'éclairer. Il a donc fallut utilisiser le shader.
![alt text](./dataMd/gridAvecSolNoir.png)

Voici ce que ça donne avec l'ajout de lumière.
![alt text](./dataMd/gridFondEclairageOk.png)
### Complétez la classe PlaneCollider qui en dérive pour détecter et réponde aux collisions entre les masses et ce plan.
Pour détecter la collision des particules de notre tissu avec le sol, il faut utiliser l'équation paramétrique du plan, à partir de la normal du plan et d'un point le composant je peux savoir si la particule est un point du plan.

Le vecteur normal du plan: n = (a,b,c) 

Le point du plan : (x0,y0,z0)

Le point de la particule : (x,y,z)

Si l'équation du plan : a(x−x0)+b(y-y0)+c(z-z0) = 0

Alors la particule appartient au plan.
Nous choisissons plutôt que ce soit si c'est inférieur à 1e-3 pour nous laisser une marge.

Si la particule est en dessous de ce 1e-3, il faut la replacer en fonction de sa vitesse de collision avec le plan et du vecteur normal du plan. 

Au début de la simulation le tissu tombe tout droit est reste raide car  il n'y a aucune autre force qui lui est appliqué.

![alt text](./dataMd/tissuTombeToutdroit.png)
![alt text](./dataMd/TissuTombetoutdroitAutreVue.png)

Ensuite, lorsque je bouge le modèle, il s'écrase bien au sol mais il le traverse partiellement et j'ai fini par trouver pourquoi.

![alt text](./dataMd/ArrétéParLePlan.png)
![alt text](./dataMd/TraverseLeSol.png)

Avant j'avais juste modifié la vitesse des particules, en leur donnant pour vitesse :

Vt = V - Vn

Vn = (n.V)n

V = Vt - Vn

V = V - (n.V)n - epsilon (n.V)n

![alt text](./dataMd/ModifVitesseColitionVecteurs.png)

Il a donc fallut modifier la position des particules qui avaient déjà dépassée le plan. En les plaçant juste au dessus de la surface du plan (fixing). On triche mais l'oeil humain de le voit pas.

p = p+epsilon* n;

![alt text](./dataMd/TissuQuiTombeSuperBien.png)
![alt text](./dataMd/TissuQuiTombeSuperBienDeProfil.png)






## 9. Chargez le maillage 'cloth.obj' représentant un vêtement. Utilisez-le pour instancier un nouveau réseau de masses-ressorts remplaçant la grille. Plutôt que les ressorts, affichez le maillage du vêtement avec le modèle d'éclairage de Blinn-Phong. Réalisez le couplage entre le modèle physique et le modèle visuel en mettant à jour les positions du maillage après la simulation.

J'ai réussi instancier un nouveau réseau de masses-ressorts sur le T-shirt et j'ai lié les particules avec le maillage en faisant la mise à jour demandé.

![alt text](./dataMd/TshirtEcraséSol.png)


## 10. Chargez le maillage du mannequin 'girl.obj' et affichez-le. Essayez de gérer les collisions entre le vêtement et ce maillage. Exploitez la BVH pour accélérer les tests de collision.

Pourtant je n'ai pas réussi à utilisé BVH, théoriquement je comprends comment cela fonctionne, le fait de faire des boîte dans les-quelles les mesh sont et grâce à ça détecter les colisions lorsque deux meshs sont dans la même boîte. J'ai essayé de créer une solution pour que le T-shirt reste malgrès tout sur le modèle girl, en y rajoutant des Collider, j'ai essayé de mettre une doite sur les épaules du modèle mais cela n'a pas fonctionné.
![alt text](./dataMd/Girl&T-ShirtNoColi.png)
![alt text](./dataMd/T-shirtSol.png)
