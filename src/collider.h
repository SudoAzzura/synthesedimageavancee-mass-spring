#pragma once

#include "particles.h"

class Collider {
public:
  virtual ~Collider() {}
  virtual void collision(Particle *p) = 0;
};

class PlaneCollider : public Collider {
public:
  Vector3d pos;
  Vector3d n;
  double kr;
  ~PlaneCollider() {}
  PlaneCollider(Vector3d position, Vector3d normal, double kr)
      : pos(position), n(normal), kr(kr) {}
  void collision(Particle *pos);
};

class RightCollider : public Collider {
public:
  Vector3d pos1;
  Vector3d pos2;
  double kr;
  ~RightCollider() {}
  RightCollider(Vector3d position1, Vector3d position2, double kr)
      : pos1(position1), pos2(position2), kr(kr) {}
  void collision(Particle *pos);
};
