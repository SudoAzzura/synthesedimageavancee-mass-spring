#include "viewer.h"
#include "camera.h"
#include "collider.h"

#include <SOIL2.h>
#include <imgui.h>
#include <pmp/algorithms/SurfaceNormals.h>

using namespace Eigen;
using namespace pmp;

Viewer::Viewer() {}

Viewer::~Viewer() {}

////////////////////////////////////////////////////////////////////////////////
// GL stuff

// initialize OpenGL context
void Viewer::init(int w, int h) {
  _winWidth = w;
  _winHeight = h;

  // set the background color, i.e., the color used
  // to fill the screen when calling glClear(GL_COLOR_BUFFER_BIT)
  glClearColor(1.f, 1.f, 1.f, 1.f);

  // 8. sol
  Mesh *ground = new Mesh();
  ground->createGrid(2, 2);
  ground->init();
  ground->transformationMatrix() = AngleAxisf(M_PI / 2.f, Vector3f(-1, 0, 0)) *
                                  Scaling(20.f, 20.f, 0.f) *
                                  Translation3f(-0.5, -0.5, -0.5);
  _shapes.push_back(ground);
  _specularCoef.push_back(0.2f);
  
  //9. 
  Mesh *cloth = new Mesh();
  cloth->load(DATA_DIR "/models/cloth.obj");
  cloth->init();
  //cloth->transformationMatrix()= Translation3f(0,0,0);
  _shapes.push_back(cloth);
  _specularCoef.push_back(0.75);
  
  Mesh *girl = new Mesh();
  girl->load(DATA_DIR "/models/girl.obj");
  girl->init();
  _shapes.push_back(girl);
  _specularCoef.push_back(0.75);
  //girl->instanciateParticleSystem(_psys);

  glEnable(GL_LINE_SMOOTH);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  loadProgram();

  _psys.init();
  
  //8. lumière utilisée pour éclairer le sol
  _lightColor = Vector3f(1.f, 1.f, 1.f);
  _lightPos = Vector4f(1.f, 0.75f, 1.f, 1.f).normalized();


    AlignedBox3f aabb;
  for (size_t i = 0; i < _shapes.size(); ++i)
    aabb.extend(_shapes[i]->boundingBox());

  _cam.setSceneCenter(aabb.center());
  _cam.setSceneRadius(aabb.sizes().maxCoeff());
  _cam.setSceneDistance(_cam.sceneRadius() * 3.f);
  _cam.setMinNear(0.1f);
  _cam.setNearFarOffsets(-_cam.sceneRadius() * 100.f,
                         _cam.sceneRadius() * 100.f);
  _cam.setScreenViewport(AlignedBox2f(Vector2f(0.0, 0.0), Vector2f(w, h)));
}

void Viewer::reshape(int w, int h) {
  _winWidth = w;
  _winHeight = h;
  _cam.setScreenViewport(AlignedBox2f(Vector2f(0.0, 0.0), Vector2f(w, h)));
  glViewport(0, 0, w, h);
}

void Viewer::draw() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  _simplePrg.activate();
  glUniformMatrix4fv(_simplePrg.getUniformLocation("projection_matrix"), 1,
                     GL_FALSE, _cam.computeProjectionMatrix().data());
  glUniformMatrix4fv(_simplePrg.getUniformLocation("view_matrix"), 1, GL_FALSE,
                     _cam.computeViewMatrix().data());
  glUniformMatrix4fv(_simplePrg.getUniformLocation("model_matrix"), 1, GL_FALSE,
                     _psys.transformationMatrix().data());
  glUniform3fv(_simplePrg.getUniformLocation("col"), 1, Vector3f::Zero().eval().data());
  _psys.draw(_simplePrg);
  _simplePrg.deactivate();

  // 8. pour le sol et la lumière avec le shader blinnPrg
  _blinnPrg.activate();
  glUniform4fv(_blinnPrg.getUniformLocation("light_pos"), 1,
                (_cam.computeViewMatrix() * _lightPos).eval().data());
  glUniform3fv(_blinnPrg.getUniformLocation("light_color"), 1,
                _lightColor.data());
  glUniformMatrix4fv(_blinnPrg.getUniformLocation("projection_matrix"), 1,
                      GL_FALSE, _cam.computeProjectionMatrix().data());
  glUniformMatrix4fv(_blinnPrg.getUniformLocation("view_matrix"), 1, GL_FALSE,
                      _cam.computeViewMatrix().data());
  for (size_t i = 0; i < _shapes.size(); ++i) {
    glUniformMatrix4fv(_blinnPrg.getUniformLocation("model_matrix"), 1,
                        GL_FALSE, _shapes[i]->transformationMatrix().data());
    Matrix3f normal_matrix =
        (_cam.computeViewMatrix() * _shapes[i]->transformationMatrix())
            .linear()
            .inverse()
            .transpose();
    glUniformMatrix3fv(_blinnPrg.getUniformLocation("normal_matrix"), 1,
                        GL_FALSE, normal_matrix.data());
    glUniform1f(_blinnPrg.getUniformLocation("specular_coef"),
                _specularCoef[i]);
    _shapes[i]->draw(_blinnPrg);
  }
  _blinnPrg.deactivate();
}



void Viewer::updateScene() {
  if (_resetSimu) {
    _psys.clear();
    //_psys.makeGrid();
    
    _shapes[1]->instanciateParticleSystem(_psys);
    // _shapes[2]->updateBVH();
    // _shapes[2]->bounds();


    _mouseForce = new AnchorForce(nullptr, Vector3d(0, 0, 0), 1000, 1);
    _psys.forces.push_back(_mouseForce);
    _psys.forces.push_back(new DragForce(&_psys, 0.01));
    _psys.forces.push_back(new GravityForce(&_psys, Vector3d(0, -9.807, 0)));

    _psys.updateGL(true);

    _cam.setSceneCenter(_psys.boundingBox().center());
    _cam.setSceneRadius(_psys.boundingBox().sizes().maxCoeff());
    _cam.setSceneDistance(_cam.sceneRadius() * 3.f);
    _cam.setMinNear(0.1f);
    _cam.setNearFarOffsets(-_cam.sceneRadius() * 100.f,
                            _cam.sceneRadius() * 100.f);
    _cam.setScreenViewport(
        AlignedBox2f(Vector2f(0.0, 0.0), Vector2f(_winWidth, _winHeight)));
    _resetSimu = false;

  }

  if (_simulation) {
    double currentTime = glfwGetTime();
    while (glfwGetTime() - currentTime < 1.f / 60.f) {
      _psys.step(_dt, _integration);
    }
    _psys.updateGL();
    _shapes[1]->updatePos(_psys);
    _shapes[1]->updateAll();
  }
  
  draw();
  
}

void Viewer::loadProgram() {
  _blinnPrg.loadFromFiles(DATA_DIR "/shaders/blinn.vert",
                          DATA_DIR "/shaders/blinn.frag");
  _simplePrg.loadFromFiles(DATA_DIR "/shaders/simple.vert",
                           DATA_DIR "/shaders/simple.frag");
}

void Viewer::updateGUI() {
  ImGui::Checkbox("Simulation", &_simulation);
  if (ImGui::Button("Reset"))
    _resetSimu = true;
  const char *items[] = {"Explicit Euler", "Mid-Point", "Backward Euler"};
  ImGui::Combo("integration", (int *)&_integration, items, 3);
  ImGui::InputFloat("dt", &_dt, 0.0001f, 0.f, "%.4f");
}

////////////////////////////////////////////////////////////////////////////////
// Events

/*
   callback to manage mouse : called when user press or release mouse button
   You can change in this function the way the user
   interact with the system.
 */
void Viewer::mousePressed(GLFWwindow *window, int button, int action,
                          int mods) {
  if (action == GLFW_PRESS) {
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
      if (mods == GLFW_MOD_CONTROL) {
        Matrix4f proj4 = _cam.computeProjectionMatrix();
        Matrix4f view4 = _cam.computeViewMatrix();
        Matrix4f C = view4.inverse();
        Matrix3f proj3;
        proj3 << proj4.topLeftCorner<2, 3>(), proj4.bottomLeftCorner<1, 3>();
        Vector2f proj_pos = Vector2f(
            2.f * float(_lastMousePos[0] + 0.5f) / float(_winWidth) - 1.0,
            -(2.f * float(_lastMousePos[1] + 0.5f) / float(_winHeight) - 1.0));
        // find nearest particle
        double dmin = 0.2; // ignore particles farther than 0.2
        for (int i = 0; i < _psys.particles.size(); i++) {
          Particle *p = _psys.particles[i];
          Vector4f pos;
          pos << p->x.cast<float>(), 1.f;
          Vector4f pos_p = (proj4 * view4 * pos);
          Vector3f dir;
          dir << pos_p.head<2>() / pos_p[3] - proj_pos, 0;
          double d = dir.head<2>().norm();
          if (d < dmin) {
            _mouseForce->x =
                p->x - (C.topLeftCorner<3, 3>() * (proj3.inverse() * dir))
                           .cast<double>();
            _mouseForce->p = p;
            dmin = d;
          }
        }
        _mod = true;
      } else {
        _cam.startRotation(_lastMousePos);
      }
    } else if (button == GLFW_MOUSE_BUTTON_RIGHT) {
      _cam.startTranslation(_lastMousePos);
    }
    _button = button;
  } else if (action == GLFW_RELEASE) {
    if (!_mod) {
      if (_button == GLFW_MOUSE_BUTTON_LEFT) {
        _cam.endRotation();
      } else if (_button == GLFW_MOUSE_BUTTON_RIGHT) {
        _cam.endTranslation();
      }
    }
    _mouseForce->p = nullptr;
    _mod = false;
    _button = -1;
  }
}

/*
   callback to manage mouse : called when user move mouse with button pressed
   You can change in this function the way the user
   interact with the system.
 */
void Viewer::mouseMoved(int x, int y) {
  if (_button == GLFW_MOUSE_BUTTON_LEFT) {
    if (_mod) {
      Matrix4f proj4 = _cam.computeProjectionMatrix();
      Matrix4f view4 = _cam.computeViewMatrix();
      Matrix4f C = view4.inverse();
      Matrix3f proj3;
      proj3 << proj4.topLeftCorner<2, 3>(), proj4.bottomLeftCorner<1, 3>();

      Vector2f proj_pos =
          Vector2f(2.f * float(x + 0.5f) / float(_winWidth) - 1.0,
                   -(2.f * float(y + 0.5f) / float(_winHeight) - 1.0));

      Particle *p = _mouseForce->p;
      if (p) {
        Vector4f pos;
        pos << p->x.cast<float>(), 1.f;
        Vector4f pos_p = (proj4 * view4 * pos);
        Vector3f dir;
        dir << pos_p.head<2>() / pos_p[3] - proj_pos.head<2>(), 0;
        _mouseForce->x =
            p->x -
            (C.topLeftCorner<3, 3>() * (proj3.inverse() * dir)).cast<double>();
      }
    } else {
      _cam.dragRotate(Vector2f(x, y));
    }
  } else if (_button == GLFW_MOUSE_BUTTON_RIGHT) {
    _cam.dragTranslate(Vector2f(x, y));
  }
  _lastMousePos = Vector2f(x, y);
}

void Viewer::mouseScroll(double x, double y) {
  _cam.zoom((y > 0) ? 1.1 : 1. / 1.1);
}

/*
   callback to manage keyboard interactions
   You can change in this function the way the user
   interact with the system.
 */
void Viewer::keyPressed(int key, int action, int mods) {
  if (key == GLFW_KEY_R && action == GLFW_PRESS) {
    loadProgram();
  } else if (key == GLFW_KEY_S && action == GLFW_PRESS) {
    _simulation = !_simulation;
  }
}

void Viewer::charPressed(int key) {}
