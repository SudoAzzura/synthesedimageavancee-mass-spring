#pragma once

#include <Eigen/Dense>

using namespace Eigen;

class ParticleSystem;
class Particle;

class Force {
public:
  virtual ~Force() {}
  virtual void addForces() = 0;
};

class GravityForce : public Force {
public:
  virtual ~GravityForce() {}
  ParticleSystem *ps; // apply gravity to all particles
  Vector3d g;         // gravity
  GravityForce(ParticleSystem *ps, const Vector3d &g) : ps(ps), g(g) {}
  void addForces();
};

class DragForce : public Force {
public:
  virtual ~DragForce() {}
  ParticleSystem *ps; // apply drag to all particles
  double kd;          // drag coefficient viscosité
  DragForce(ParticleSystem *ps, double kd) : ps(ps), kd(kd) {}
  void addForces();
};

class SpringForce : public Force {
  // connects two particles by a spring
public:
  virtual ~SpringForce() {}
  Particle *p0, *p1; // particles
  double ks, kd;     // spring constant = constante de rigidité du ressort, damping coefficient = coefficient d'amortissement
  double l0;         // rest length = longueur du ressort
  SpringForce(Particle *p0, Particle *p1, double ks, double kd, double l0)
      : p0(p0), p1(p1), ks(ks), kd(kd), l0(l0) {}
  void addForces();
};

class AnchorForce : public Force {
  // attaches a particle to a fixed point by a spring
public:
  virtual ~AnchorForce() {}
  Particle *p;   // particle
  Vector3d x;    // point to anchor it to
  double ks, kd; // spring constant, damping coefficient
  AnchorForce(Particle *p, Vector3d x, double ks, double kd)
      : p(p), x(x), ks(ks), kd(kd) {}
  void addForces();
};
