#include "collider.h"

using namespace Eigen;

const double epsilon = 1e-3;

void PlaneCollider::collision(Particle *p) {
    double distcol = n.x() * (p->x.x() - pos.x())+n.y() * (p->x.y() - pos.y()) + n.z() * (p->x.z() - pos.z());
    double vitcol = p->v.x() * n.x() + p->v.y() * n.y() + p->v.z() + n.z();
    if(distcol < epsilon && vitcol < epsilon) {
        //il y a une colision & il faut déplasser la particule
        p->v.x() = p->v.x() -(n.x() * p->v.x())*n.x() - epsilon*(n.x() * p->v.x())*n.x();
        p->v.y() = p->v.y() -(n.y() * p->v.y())*n.y() - epsilon*(n.y() * p->v.y())*n.y();
        p->v.z() = p->v.z() -(n.z() * p->v.z())*n.z() - epsilon*(n.z() * p->v.z())*n.z();

        p->x.x() = p->x.x() +epsilon * n.x();
        p->x.y() = p->x.y() +epsilon * n.y();
        p->x.z() = p->x.z() +epsilon * n.z();    
    }
    double nf = n.x() * p->f.x() + n.y() * p->f.y() + n.z() * p->f.z();
    if(nf <0) {
        Vector3d fc =Vector3d(0,0,0);
        fc.x() = n.x() * p->f.x() * n.x();
        fc.z() = n.z() * p->f.z() * n.z();
        fc.y() = n.y() * p->f.y() * n.y();
        p->f -=  fc;
        
    }
}
void RightCollider::collision(Particle *p) {
    double m = (pos1.z() - pos2.z())/ (pos1.x() - pos2.x());
    double b = pos1.z() - m * pos1.x();
    double res = p->x.x() * m +b;
    if( res == p->x.z()) {
        Vector3d fc =Vector3d(0,0,0);
        fc.x() = 0 * p->f.x() * 0;
        fc.z() = 0 * p->f.z() * 0;
        fc.y() = 1 * p->f.y() * 1;
        p->f -=  fc;
    }
}
