#include "particles.h"
#include "forces.h"
#include "collider.h"
#include "mesh.h"

#include <iostream>

using namespace std;
using namespace Eigen;

void makeGrid(ParticleSystem *psys);

void ParticleSystem::init() {
  // OpenGL allocation
  glGenVertexArrays(2, _vao);
  glGenBuffers(2, _vbo);
  _ready = true;
}

void ParticleSystem::updateGL(bool first) {
  // Particles
  vector<Vector3f> positions;
  positions.resize(particles.size());
  _bbox.setEmpty();
  for (size_t i = 0; i < particles.size(); i++) {
    positions[i] = particles[i]->x.cast<float>();
    _bbox.extend(positions[i]);
  }
  glBindVertexArray(_vao[0]);
  glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);
  if (first)
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vector3f) * particles.size(),
                 positions.data(), GL_DYNAMIC_DRAW);
  else
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vector3f) * particles.size(),
                    positions.data());
  glBindVertexArray(0);

  // Spring forces
  positions.resize(0);
  positions.reserve(forces.size() * 2);
  for (size_t i = 0; i < forces.size(); i++) {
    SpringForce *s = dynamic_cast<SpringForce *>(forces[i]);
    if (s) {
      positions.push_back(s->p0->x.cast<float>());
      positions.push_back(s->p1->x.cast<float>());
    }
    AnchorForce *a = dynamic_cast<AnchorForce *>(forces[i]);
    if (a && a->p != nullptr) {
      positions.push_back(a->p->x.cast<float>());
      positions.push_back(a->x.cast<float>());
    }
  }

  glBindVertexArray(_vao[1]);
  glBindBuffer(GL_ARRAY_BUFFER, _vbo[1]);
  if (first || nbSpringForces != positions.size()) {
    nbSpringForces = positions.size();
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vector3f) * nbSpringForces,
                 positions.data(), GL_DYNAMIC_DRAW);
  } else
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vector3f) * nbSpringForces,
                    positions.data());
  PlaneCollider c = PlaneCollider(Vector3d(-1, 0, -1), Vector3d(0, 1, 0), 0.1);
  // RightCollider r = RightCollider(Vector3d(3, 7, 0),Vector3d(4, 7, 0),0.1);
  // RightCollider r2 = RightCollider(Vector3d(3, 7, 1),Vector3d(4, 7, 1),0.1);
  
  for (auto p : particles)
  {
    c.collision(p);
    // r.collision(p);
    // r2.collision(p);
  }
  glBindVertexArray(0);
}

ParticleSystem::~ParticleSystem() {
  if (_ready) {
    glDeleteBuffers(2, _vbo);
    glDeleteVertexArrays(2, _vao);
  }
  clear();
}

void ParticleSystem::clear() {
  for(auto f : forces)
    delete f;
  forces.resize(0);
  nbSpringForces = 0;
  for(auto p : particles)
    delete p;
  particles.resize(0);
  for(auto c : colliders)
    delete c;
  colliders.resize(0);
}

void ParticleSystem::draw(Shader &shader) {
  // Draw particles
  glBindVertexArray(_vao[0]);
  glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);

  GLuint vertex_loc = shader.getAttribLocation("vtx_position");
  glVertexAttribPointer(vertex_loc, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
  glEnableVertexAttribArray(vertex_loc);

  glPointSize(5.f);
  glDrawArrays(GL_POINTS, 0, particles.size());

  glDisableVertexAttribArray(vertex_loc);
  glBindVertexArray(0);

  // Draw spring forces
  glBindVertexArray(_vao[1]);
  glBindBuffer(GL_ARRAY_BUFFER, _vbo[1]);

  vertex_loc = shader.getAttribLocation("vtx_position");
  glVertexAttribPointer(vertex_loc, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
  glEnableVertexAttribArray(vertex_loc);

  glLineWidth(2.f);
  glDrawArrays(GL_LINES, 0, nbSpringForces);

  glDisableVertexAttribArray(vertex_loc);
  glBindVertexArray(0);
}

void ParticleSystem::step(double dt, Integration type)
{
  if (type == EULER)
  {
    explicitEulerStep(this, dt);
    ParticleSystem::updateGL();
  }
  else if (type == MIDPOINT)
  {
    midPointStep(this, dt);
    ParticleSystem::updateGL();
  }
}

int ParticleSystem::getDimensions()
{
  return 6 * particles.size();
}
// on get les x et v des particules
void ParticleSystem::getState(VectorXd &state)
{
  int pos = 0; // position
  int n = 3;   // taille d'un bloque de donnée x ou v
  for (Particle *p : particles)
  {
    state.segment(pos, n) = p->x;
    pos += n;
    state.segment(pos, n) = p->v;
    pos += n;
  }
}
// on set les particules
void ParticleSystem::setState(const VectorXd &state)
{
  int pos = 0; // position
  int n = 3;   // taille d'un bloque de donnée x ou v
  for (Particle *p : particles)
  {
    p->x = state.segment(pos, n);
    pos += n;
    p->v = state.segment(pos, n);
    pos += n;
  }
}

void ParticleSystem::getDerivative(VectorXd &deriv)
{
  // mise à 0 des forces
  for (Particle *p : particles)
  {
    p->f = Vector3d(0, 0, 0);
  }
  for (Force *force : forces)
  {
    force->addForces();
  }

  int pos = 0; // position
  int n = 3;   // taille d'un bloque de donnée x ou v
  for (Particle *p : particles)
  {
    deriv.segment(pos, n) = p->v;
    pos += n;
    deriv.segment(pos, n) = p->f / p->m;
    pos += n;
  }
}

void ParticleSystem::makeGrid(int m, int n) {
  double mass = 1;                               // total mass
  double pmass = mass / ((m + 1) * (n + 1));     // mass per particle
  double x0 = 0.3, x1 = 0.7, y0 = 0.3, y1 = 0.7; // extent of rectangle
  double dx = (x1 - x0) / m, dy = (y1 - y0) / n; // lengths of springs
  double dd = sqrt(dx * dx + dy * dy);
  double ks = 200, kd = 0.1; // spring constant, damping
  double ks_diag = 50;
  // create particles
  Eigen::Array<Particle *, Dynamic, Dynamic> particlesTab(m + 1, n + 1);
  for (int i = 0; i <= m; i++)
  {
    for (int j = 0; j <= n; j++)
    {
      Vector3d x(x0 + dx * i, y0 + dy * j, 0);
      Vector3d v(0, 0, 0);
      int index = particles.size();
      particlesTab(i, j) = new Particle(index, pmass, x, v);
      particles.push_back(particlesTab(i, j));
    }
  }

  // create anchor points
  // forces.push_back(
  //     new AnchorForce(particlesTab(0, n), particlesTab(0, n)->x, 1000, 0.1));
  // forces.push_back(
  //     new AnchorForce(particlesTab(m, n), particlesTab(m, n)->x, 1000, 0.1));

  // create springs
  for (int i = 0; i <= m; i++)
  {
    for (int j = 0; j <= n; j++)
    {
      Particle *p0 = particlesTab(i, j);
      if (i < m)
      {
        Particle *p1 = particlesTab(i + 1, j);
        forces.push_back(new SpringForce(p0, p1, ks, kd, dx));
      }
      if (j < n)
      {
        Particle *p1 = particlesTab(i, j + 1);
        forces.push_back(new SpringForce(p0, p1, ks, kd, dy));
      }
      // TODO: add shear springs
      double dhyp = sqrt(pow(dx, 2.0) + pow(dy, 2.0));
      if (i < m && j < n)
      {
        Particle *p2 = particlesTab(i + 1, j + 1);
        forces.push_back(new SpringForce(p0, p2, ks, kd, dhyp));
      }

      if (i > 0 && j < n)
      {
        Particle *p2 = particlesTab(i - 1, j + 1);
        forces.push_back(new SpringForce(p0, p2, ks, kd, dhyp));
      }
    }
  }
}
