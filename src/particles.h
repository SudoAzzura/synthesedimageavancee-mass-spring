#pragma once

#include "integration.h"
#include "shader.h"
#include "mesh.h"

#include <Eigen/Dense>
#include <vector>

using namespace Eigen;

class Force;
class Collider;

class Particle {
public:
  int i;      // index
  Vector3d x; // position
  Vector3d v; // velocity
  Vector3d f; // force
  double m;   // mass
  Particle(int i, double m, Vector3d x, Vector3d v) : i(i), x(x), v(v), m(m) {}
};

class ParticleSystem : public ODESystem {
public:
  ParticleSystem()
      : _ready(false), _transformation(Eigen::Affine3f::Identity()) {}
  ~ParticleSystem();
  void init(); // initialize the system
  void step(double dt,
            Integration type); // perform a time step of length dt
  void draw(Shader &shader);   // draw everything
  void updateGL(bool first = false);
  void clear();
  void makeGrid(int m = 10, int n = 10);
  void makeMesh(Mesh * mesh);


  // ODE functions, see integration.h
  int getDimensions();
  void getState(VectorXd &state);
  void setState(const VectorXd &state);
  void getDerivative(VectorXd &deriv);

  int getDOFs();
  void getState(VectorXd &x, VectorXd &v);
  void setState(const VectorXd &x, const VectorXd &v);
  void getForces(VectorXd &f);

  const Eigen::Affine3f &transformationMatrix() const {
    return _transformation;
  }

  Eigen::Affine3f &transformationMatrix() { return _transformation; }
  
  const Eigen::AlignedBox3f &boundingBox() const { return _bbox; }

  std::vector<Particle *> particles;
  std::vector<Force *> forces;
  std::vector<Collider *> colliders;

private:
  unsigned int _vao[2];
  unsigned int _vbo[2];
  size_t nbSpringForces;
  Eigen::AlignedBox3f _bbox;
  Eigen::Affine3f _transformation;
  bool _ready;
};
