#include "integration.h"

void explicitEulerStep(ODESystem *system, double dt) {
  int dim = system->getDimensions();
  VectorXd state(dim);
  VectorXd deriv(dim);
  system->getState(state);
  system->getDerivative(deriv);
  state = state + dt * deriv;
  system->setState(state);
}

void midPointStep(ODESystem *system, double dt) {
  int dim = system->getDimensions();
  VectorXd state(dim);
  system->getState(state);
  ODESystem * system2 = system;
  explicitEulerStep(system2, dt/2);
  VectorXd deriv2(dim);
  system2->getDerivative(deriv2);
  state = state + dt * deriv2;
  system->setState(state);
}
