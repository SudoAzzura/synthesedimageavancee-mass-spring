#include "forces.h"
#include "particles.h"
//gravité terrestre
void GravityForce::addForces() {
  for(Particle *p : ps->particles) {
    //masse de la particule * la gravité
    p->f += p->m * g;
    //p->f += 0.05 * g;

  }
}
//amortissement visqueux
void DragForce::addForces() {
  kd = 1;
  
  for(Particle *p : ps->particles) {
    // coef de viscosité * la vitesse 
    // fois -1 pcq ça diminue l'nrj du systeme
    p->f += -kd * p->v;
  }
}
//force du ressort
void SpringForce::addForces() {

  kd = 1;
  ks = 100;
  Vector3d l = p0->x - p1->x;
  Vector3d vdif = p0->v - p1->v;
  double force1 = vdif.x()*l.x() + vdif.y()*l.y() + vdif.z()*l.z();//produit scalair à la main
  Vector3d force = (-kd) * (force1/l.norm()) * (l/l.norm());
  Vector3d force3 =  -ks * ( l.norm() - l0) * (l/l.norm());
  //F_i,j 
  p0->f += force3;
  p1->f += -force3;
  p0->f += force;
  p1->f += -force;

}
// for d'acroche
void AnchorForce::addForces() {
  // kd = 0.5;
  // ks = 100;
  if (p == NULL)
    return;
  Vector3d dx = p->x - x;
  p->f += -ks * dx - kd * p->v;
}